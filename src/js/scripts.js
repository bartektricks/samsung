const burgerBtn = document.querySelector('.burger-wrapper');
const navigation = document.querySelector('.nav-menu-wrapper');
const universityBtn = document.querySelectorAll('.is-university');
const universityAmbassador = document.querySelectorAll('.ambassador-popup-element');
const universityCloseBtn = document.querySelectorAll('.close-btn');
const newsBtn = document.querySelectorAll('.is-news');
const newsPopupElement = document.querySelectorAll('.news-popup-element');

$('.scrollTo').on('click', function(evt){
  const id = $(this).attr('href').replace('#','');
  const item = $("[data-scroll='"+ id +"']");
  const itemOffset = item.position().top;
  evt.preventDefault();
  burgerBtn.classList.remove('is-active');
  navigation.classList.remove('is-active');
  document.body.classList.remove('is-menu-open');

  $('html, body').animate({scrollTop: itemOffset}, 'slow');
});

$(document).ready(function() {
  new Vivus('svg-header', {duration: 120});
  new Vivus('block-svg-1', {duration: 120});
  new Vivus('block-svg-2', {duration: 120});

  var animateonScroll = function() {
    var elems;
    var windowHeight;
    function init() {
      elems = document.querySelectorAll('.hidden');
      windowHeight = window.innerHeight;
      addEventHandlers();
      checkPosition();
    }
    function addEventHandlers() {
      window.addEventListener('scroll', checkPosition);
      window.addEventListener('resize', init);
    }
    function checkPosition() {
      for (var i = 0; i < elems.length; i++) {
        var positionFromTop = elems[i].getBoundingClientRect().top;
        if (positionFromTop - windowHeight <= 0) {
          elems[i].className = elems[i].className.replace(
            'hidden',
            'show-on-scroll'
          );
        }
      }
    }
    return {
      init: init
    };
  };
  animateonScroll().init();
});

burgerBtn.addEventListener('click', () => {
  burgerBtn.classList.toggle('is-active');
  navigation.classList.toggle('is-active');
  document.body.classList.toggle('is-menu-open');
});

function universityScroll(element) {
  if(screen.width < 1024) {
    const id = element.getAttribute('href').replace('#', '');
    const item = $("[data-scroll='"+ id +"']");
    const itemOffset = item.offset().top - 40;
    $('html, body').animate({scrollTop: itemOffset}, 'slow');
  }
}

function closeUniversityModal(e) {
  e.preventDefault();

  for(var i = 0; i < universityAmbassador.length; i++) {
    if(universityAmbassador[i].classList.contains('is-active')) {
      universityAmbassador[i].classList.remove('is-active');
    }
  }
}

$('.close-btn').each(function(index){
  universityCloseBtn[index].addEventListener('click', e => {
    closeUniversityModal(e);
    universityScroll(universityCloseBtn[index]);
  });
});

$('.is-university').each(function(index) {
  universityBtn[index].addEventListener('click', e => {
    closeUniversityModal(e);

    universityAmbassador[index].classList.toggle('is-active');
    universityScroll(universityBtn[index]);
  });
});

$('.popup-heading-link').each(function(index){
  $(this).on('click', function(e){
    $('.popup-heading-link').removeClass('is-active');
    e.preventDefault();
    $(this).addClass('is-active');

    if($('.popup-text').data('warsaw')) {

      $('.popup-text').removeClass('is-active');
      $('.popup-image').removeClass('is-active');
      $('.popup-text').eq(index).addClass('is-active');
      $('.popup-image').eq(index).addClass('is-active');

    }
  });
});

$('.is-news').each( function(index) {
  newsBtn[index].addEventListener('click', e => {
    e.preventDefault();

    $('.news-popup-element').each( function(i) {
      if(newsPopupElement[i].classList.contains('is-active')) {
        newsPopupElement[i].classList.remove('is-active');
      }
    });

    newsPopupElement[index].classList.toggle('is-active');
  });
});

$('.close-news').each(function(index) {
  $(this).on('click', function() {
    if(newsPopupElement[index].classList.contains('is-active')) {
      newsPopupElement[index].classList.remove('is-active');
    }
  })
});

/* Slick */
$('.news-list').slick({
  infinite: false,
  slidesToShow: 3,
  slidesToScroll: 1,
  prevArrow: $('.news-previous'),
  nextArrow: $('.news-next'),

  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        inifinite: false,
      }
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false
      }
    }
  ]
});

$('.three-icons-wrapper').slick({
  infinite: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  prevArrow: $('.slider-ico-previous'),
  nextArrow: $('.slider-ico-next'),

  responsive: [
    {
      breakpoint: 1359,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        inifinite: false,
      }
    },
    {
      breakpoint: 4000,
      settings: 'unslick',
    }
  ]
});
